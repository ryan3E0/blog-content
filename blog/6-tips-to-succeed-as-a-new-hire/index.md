---
title: 6 Tips to Succeed as a New Hire
date: 2019-11-24
description: Starting at a new company can be difficult, here are some tips for how to get up to speed at a new job.
coverImage: cover.jpg
coverImageAltText: Laptop with a code editor open
---

Starting at a new company can be difficult, here are some tips for how to get up to speed at a new job.

## 💻 1. Independent Problem Solving

Even though you have a team to support you and collaborate with, working independently is an important skill to develop.

### Understand the Problem

Understanding is the most important step in problem-solving. Often developers can pressure themselves into diving into code to complete a task quickly. Rushing to implement a solution typically does not result in quality work. It will have bugs and it may not address the needs of the request. It is important to take a step back and try to understand the problem before starting. It is tempting to immediately ask an experienced developer, but initially working independently will help you gain more experience in the environment. If your first inclination is to ask someone, that might become a habit that you come to rely on, which can affect your job performance in the future. The more you practice independently, the better you will retain information that you can use in future tasks. Use any resources available to you to understand the problem. Take some time to try to understand what is being asked of you.

#### Use All Available Resources

Because you are new to a company, you may not immediately understand the surrounding context of a problem. It is important to figure out where to look to find additional information. The task assigned to you may not contain every detail, so you may have to piece together the information on your own.

- Read the request in full.
  - Do not start programming based on your assumption of the problem, you might miss important details.
- Search your company's documentation and look for anything related to your task and read it to understand the context.
- Use your company's issue tracker to look for old issues that are related.
  - Someone likely worked in that area before and you may find clues on how they approached it.
- If you are copied on email threads, search for any that are related to the task and read them.
  - There might be discussions that only happened over email. While it is not ideal for that to happen, it can be a resource to explore to get additional background information.
- Test the application in a development environment.
  - Gain knowledge of how the application functions and what users expect. That will help ensure that your solution is consistent with the rest of the application.
  - If you are fixing a bug, recreate it so that you have a case to test your solution against.
- Review the code.
  - Try to find all of the files related to the area you are working on. Figure out where the data originates, how gets passed into functions, and how it is displayed to the user.

### Create a Plan

After you have taken the time to understand the problem, create a plan on how you want to solve that problem. This can be a basic to-do list of steps that you can incrementally work on and check off as you complete them. You don't have to spend too much time on this, the list does not need to go into great detail. It can contain basic steps such as "Create a function to get the user's profile" or "Add a view to display the user profile", but you can leave out the implementation details. It doesn't have to be perfect, you may find things to add as you work, but try to make it as complete as possible upfront.

This plan will help you get organized to work through the problem by:
1. Breaking it down into manageable pieces.
2. Allowing you to visualize and feel good about your progress as you check off items.
3. Ensuring that all the needs of the task are met.
4. Gauging the amount of work before you get too far into it.

#### Get the Plan Reviewed

If you are unsure about your plan, ask a teammate to review it. Your team can identify things missing or suggest alternative approaches. Because you have put in the effort to try to understand the problem and plan out your solution, your teammates will appreciate it and will be better able to help you. It is easier to review a plan and provide feedback than it is to explain the background while trying to formulate an approach.

If your plan also seems like more work than you anticipated, you can get it reviewed to ensure you are not misinterpreting the scope of the task. It may have been underestimated, so you should talk to your team lead. They can help correct your understanding of the task or permit you to proceed with the larger solution. This will help put your mind at ease because you won't be stuck working on a large task without being sure of how long it will take.

### Execute the Plan

Now that you have a plan, it's time to write the code. Because you have taken the time to understand the problem and plan out a solution, this part will go a lot smoother.

#### Scaffold the Solution

When writing code, create a structure using function definitions and comments.

- Write the function definitions first and include arguments and return values.
  - This helps account for each piece needed for the solution.
- Once the functions are planned out, add comments with a description of them.
  - This helps ensure that functions do _one_ thing. When describing the function, if a series of descriptions are necessary, this is a trigger to break it up into separate functions.

#### Make it Work

After you have a scaffold of the code, it is time to make it work. Implement the functions, call them with data, and make any user interface updates.

Implementing a working solution should be the focus. It does not have to be perfect and it does not have to solve every potential future problem. If you try to prematurely optimize or modularize the code, it may take a lot of time to write and it may not solve the problem. A more complex approach will also need to be tested more thoroughly.

#### Make it Work _Better_

Once you have a working solution, take some time to improve it.

- Move any repeated code into functions.
- Clean up unused variables.
- Ensure variables have an initial value or that they are checked for existence to avoid undefined variable errors.
- Fix code formatting.
- Test a variety of cases.
  - Test with expected values, unexpected values (0s, empty strings, nulls), and missing values.
  - Fix any issues you find.

## 🙋‍♀️🙋‍♂️ 2. Ask for Help

If you have put some effort into a task but are still lost, now is the time to ask for help. When asking for help, it is important to be considerate of others.

### Ask for a Meeting

It is tempting to walk down the hall and ask someone a question, but this can be inconsiderate to the other person. It may seem insignificant to ask them a quick question, but you are interrupting that person from the work they are focused on. If you are there in person, they feel obligated to help you at that moment. That person could be getting interrupted throughout the day and it impacts their work. Teammates want to help you, but it can be distracting at times, so keep that in mind.

Be considerate by reaching out to set up a meeting. As soon as you know you will need help, figure out the right person to ask and send them a message asking for their time. Give them a time window when you would like to meet and how much time you expect it to take. This gives them a chance to reach a good stopping point before switching their focus.

### Provide Background Information

To get the best quality response, give the other person a heads-up on what you would like to meet about. This does not have to be too detailed, usually a link to the task and a summary. This gives them time to think about it or review any documentation before they meet with you.

Without background information, the discussion can be unproductive. The other person may have some crucial knowledge to get you on the right path, but if they are only hearing the problem from your point of view, that information may not surface in the discussion. This may not get you closer to solving your problem.

### Validate Information

It is important to take the information given to you and validate that it is correct, then decide what to do. Experienced teammates know a lot, but they do not know everything. Things may have changed since they last looked at the code or they only have a vague recollection of how it should work. Use their information as a path to explore or a suggestion to try, but be aware that you might have to go beyond that.

## 📝 3. Write Documentation

Writing documentation may not sound like the most fun part of the job, but it goes a long way in helping you solidify your knowledge and creates a reference for your team to look to in the future.

### What to Document

#### High-level Information

Documentation does not always have to be super technical or lengthy, it only needs to be a starting point for someone. Documenting an overview, instructions to access it, details about a key feature, and information about where the code is and what it does is enough to help someone get started.

As a rule of thumb, write documentation as if the person reading it has no background knowledge of the subject. This will help ensure that new developers do not have to struggle with the same things that you did. Having a page started with a basic set of information is beneficial because others can contribute to it over time.

#### How-to Guides

If it is a repeatable set of steps, create a how-to guide.

- Make sure those steps can be completed from start to finish.
  - The purpose of a how-to guide should be that others can repeat those steps without having to ask for help or struggle with figuring out what to do. Make sure all steps are included and can be completed without intervention.
- Include links to other documentation pages when necessary.
  - If something is documented elsewhere, you do not have to rewrite it. Include links to those pages.
- Include version numbers for software.
  - A sentence like "Install Node.js" can be understood by developers, but it may lead to difficulty in completing all of the steps in the guide. If they download the latest version of Node.js, but the code runs on Node.js v10, they may encounter issues that are not covered in the guide. It can be difficult to troubleshoot without knowing that.

### Reinforce Your Knowledge

Build your knowledge by recalling the important information and documenting it after you solve a problem.

### Share Information

Avoid information silos by building the team knowledge base, your team will appreciate it. At many companies, there are certain things that only _one_ person knows and it isn't documented anywhere. If that person is not available, you are out of luck. Help the team and yourself by documenting it.

## 👩‍💼👨‍💼 4. Participate in Meetings

Meetings are an important part of any workplace. As a new hire, you may feel lost in meetings. Do the following to help you get more comfortable in meetings.

### Review the agenda

Make sure you know what the purpose of the meeting is and why you were invited. If there is no agenda, reach out to the organizer for details.

### Review Documentation

Try to gain some knowledge before going to the meeting. This will make it easier to follow the discussion and get something out of it. Without background information, it can be difficult to stay engaged in the meeting.

### Think About an Approach

If the meeting is to discuss solutions to a problem, make a list of what you would do. You do not have to share it if you do not feel confident in it, but it is good practice to think about how you would present your ideas to the team.

### Take Notes

Try to identify the important parts of the discussion and take notes for yourself. This will help you absorb the information.

If there is no designated note-taker for the meeting, you can be that person. Sharing your notes with the team will ensure that everyone has a place to reference what was discussed in the meeting. Confirm action items in the meeting and write them down so that they are not forgotten.

### Speak Up

This part can be difficult, especially being new, but it is a necessary part of being a developer. If you feel that you can contribute, speak up in meetings. As a newcomer, you can offer an alternative view. Even if you are wrong, that is okay. Others can speak to why your approach may not work, which will help you get more comfortable working in a new environment.

## ✨ 5. Contribute in Other Ways

Your company probably did not hire you to do exactly as you are told, they want you to make contributions beyond the tasks that are assigned to you.

### Propose Improvements

If you are consistently running into larger issues and you have ideas to address them, propose solutions. As someone new to the team, you may see areas of improvement that the rest of the team has become comfortable with. It is important to propose them first and get your team's input before implementing anything. If you do it on your own, it will exclude the rest of the team and it will disrupt if everyone is blind-sided by it, even if it is a good idea.

## 👍 6. Relax, You're Doing Fine

Hiring is an investment for the company, they invested in _you_. You should be proud of that. They believe that the effort spent bringing you on will be a positive return for them in the future. It will be challenging initially, but keep at it and you will eventually get more comfortable.

## Summary

1. Try to solve problems independently by understanding the problem and creating a plan before you start coding.
2. Put in the effort before asking for help and be respectful of your teammate's time.
3. Write documentation to help reinforce what you learned and to help your teammates.
4. Participate in meetings by being prepared and speaking up when you have something to contribute.
5. Contribute to areas beyond your assigned work by proposing solutions to the team.
6. Be proud of your accomplishments and don't let initial struggles bring you down, you will be fine.

I hope this was helpful, thanks for reading!