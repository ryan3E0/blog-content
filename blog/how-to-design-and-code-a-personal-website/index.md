---
title: How to Design and Code a Personal Website
date: 2019-12-09
description: Practice design through the process of designing a personal website from scratch and turning it into code.
coverImage: cover.jpg
coverImageAltText: Two people sitting at a desk with laptops collaborating with pen and paper on a design
---

Many developers believe that being good at design is an innate ability, that being creative is something you are born with. But design is a skill that can be learned like any other. You do not have to be born an artist to create a good looking website, it just takes practice. This article will cover how to practice design through the process of designing a personal website from scratch and turning it into code.

## ✨ Why Design it Yourself?

Why design it yourself when you could use a UI library like Bootstrap or a pre-made template? Below are some of the benefits of designing your website.

1. **Stand out from the crowd.** Many developer blogs use similar templates and it is easy to see that it is not a custom design. If the purpose of a personal website is to showcase your abilities then using a template may detract from that goal.
2. **Practice your craft.** Designing it will help you practice with design principles, tools, HTML, and CSS. You will get more comfortable with developing user interfaces and bringing them to the web.
3. **Better app performance.** Your websites will be lightweight and have better performance with custom CSS. If you were to include a UI library or a template, it may contain a large amount of code to cover a variety of possible customizations that you are not using. If unused code is sent to the user, it will negatively affect the performance of your website.
4. **Develop career skills.** In a web development role, you may not have to implement full website designs from scratch, but you should be able to make a decent looking interface that is consistent with existing designs. Being a "full-stack developer" often means being proficient in a back-end language or a front-end JavaScript framework while being able to fumble around with the design, HTML, and CSS. The job gets done eventually but the result may not be aesthetically pleasing, may be inconsistent with the rest of the app, or it may be inaccessible on certain devices. Full-stack developers should have some basic design knowledge and be able to provide a consistent experience to the user.
   
    ![Drawing of a horse with a detailed back-end, but a low-quality front-end](./fullStack.jpg)

    _"Full-stack Developer"_

5. **It can be fun.** Creating something you are proud of is a rewarding experience. It can be enjoyable if you put in some time to practice it. You do not have to be an artist to have fun doing it!

To get started with designing your site, take incremental steps. It is hard to visualize a design from a code editor, I recommend working in a visual design tool first, then translating that result in code. It is easier to focus on the design first so that you can see it and make adjustments without having to rewrite code.

## 🔳 Create a Wireframe

The first step is creating a low-fidelity wireframe of the site. Creating a wireframe helps by establishing the structure of the page before visual design and content are added. A wireframe does not need to be pretty, it should focus on the layout of the content. You can draw it by hand or use the basic features of a design tool.

To create a wireframe, I like to think of designs as a series of rectangles. Elements on a webpage are rectangular blocks that flow from top to bottom. Starting with rectangles does not require any artistic talent.

### Structure the Website

The elements that you put in your wireframe are up to you. You may consider adding a navigation bar, header, blog posts, and a footer. You may not need all of those elements to start, you can keep it basic and add to it later. Decide what you want to include and incorporate those sections into your wireframe. If you are having trouble with this step, you can look at a similar website, emulate how the content is structured, and modify it to fit your needs.

![Wireframe drawing of a website](./mockup.png)

The wireframe does not have to be perfect. Once you have the structure of your site in a good place, you can move to visual design.

## 🎨 Apply the Visual Design

To translate the low-fidelity wireframe into a design, you can use a free design tool such as [Figma](https://www.figma.com/). If you have never used a design program, it may take some practice to get comfortable, but you do not need to be an expert in it. Basic features such as adding rectangles, resizing, and setting properties are enough to get started.

### Implement the Layout

To get started, create a blank canvas to work on to represent a blank browser page. Implement your wireframe in the design tool by creating containers for your content. I recommend starting with black and white first so that you can focus on the layout.

Refine your layout so that the elements are sized appropriately, are aligned, and there is space between them.

![Wireframe graphic of a website](./mockup2.png)

### Add Sections and Placeholder Content

After you have it looking like a website, make it look prettier. Emulate styles from other websites by figuring out what makes the elements look appealing.

At this stage consider shape, size, borders, and shadows. Gradually upgrade the basic rectangles with a style that you like.

![A card element progressing from a basic rectangle to a styled card](./cards.png)

### Update the Typography

Fonts and spacing go a long way to making a design look polished. Even simple designs can be high-quality if the typography is done well. Again, you can emulate another website or search for font and typography resources to incorporate into your design.

![Card element with a heading, paragraph, and a link with styled text](./cardTypography.png)

### Give it Color

Next, add color to the site. Give your site character by establishing your brand. Think about how you want the site to come across to the reader. If you want it to look clean and minimal then choose colors that are not too bright, keep gradients subtle, and choose fonts that are easy to read. If you want it to look fun then choose bright colors, use vibrant gradients, apply background textures, use rounded elements, and choose fonts that stand out.

![Card element with color added](./cardColor.png)

Adding color can seem intimidating but you do not need to know color theory to do it. If your design started in black and white, you can pick a single color to accent elements to give the design some life. If you want to go beyond that, I recommend choosing one or two colors that you like, then using different lightness variations of that color. This helps create a cohesive theme without having to be an expert on colors. When selecting background and foreground colors, keep readability in mind by [checking the color contrast](https://webaim.org/resources/contrastchecker/).

An example of this is setting dark blue for the background, then using a lighter version of the same blue for the text. For white background colors, you can use a medium lightness of the blue for headings.

![Dark blue background showing light blue text](./colors1.png)

![Light background showing a medium shade blue heading](./colors2.png)

Once you have colors incorporated into your design, move on to reviewing the overall design and making adjustments.

### Refine It

As you are designing you should take a step back to look at your design as a whole and refine it. Critique your design by describing what you see in plain language, then translate that statement into the technical issue that needs to be addressed.
 - **Looks cramped?**
   - Increase padding and margins.
 - **The text is hard to read?**
   - Pick a clearer font or increase font sizes.
   - Increase the color contrast between the background and the foreground.
 - **Hard to parse the content?**
   - Add headings with a higher font-weight.
   - Add more spacing between headings and paragraphs.
 - **Looks sloppy or inconsistent?**
   - Align elements in a straight line horizontally and vertically. Setting up guides in a design program can help ensure that elements are aligned properly.
   - Adjust padding and margins to keep consistent spacing vertically.
   - Make the text consistent by establishing the font face and sizes for headers and paragraphs. Avoid having too many variations of text.
   - Ensure all colors adhere to your color palette.

![Full website design](./siteDesign.png)

Once you have your design finalized you can begin translating it into code.

## 🧱 Create the HTML Structure

Place all the HTML elements on the page, do not worry about adding CSS yet.

This will allow you to see the natural flow of the HTML document and box model. Having the structure of the page established in HTML will make it easier to know what you need to add in your CSS to position and style the elements to match the design.

![Screenshot of a browser showing unstyled HTML](./htmlBrowser.png)

After the HTML is in place, move on to the positioning and styling with CSS.

## 💅 Style it with CSS

Because HTML documents flow from top to bottom, you can start at the top of the document and work your way down. Using the design you created, try to replicate it as closely as possible in HTML & CSS.

Remember that it does not have to be all or nothing, you can implement the parts you need for the initial version of the site. For my website, I only wanted an introduction, social links, and a way to post blogs, so that is all I implemented to start with. If you aren't ready to implement a section, you can remove it until you are ready.

### Position the Sections and Elements

I recommend focusing on the layout of the elements and saving the aesthetic aspects for later. If you are laying out and styling elements at the same time, it can lead to thrashing back and forth, which can limit your progress. An example is laying out a navigation bar while trying to set font faces, weights, and sizes. You may have everything in the location you want but then adjusting the font throws everything off. That may trigger you to then modify the navigation bar to get everything to fit again. But what if the navigation bar is now too tall? You may go back and forth several times which can lead to frustration. Instead, I recommend focusing on the layout of the elements first and trying to make it work for different sizes of content. This can take some trial and error in CSS, but you want to make sure your elements have enough width to comfortably fit the content in it.

When turning a layout into a full design, I like to think of horizontal lines that slice the design into sections from top to bottom. A navigation bar at the top could be the first slice. You can focus on just the HTML and CSS needed for the navigation without worrying about sections below it. Within the navigation bar, vertical slices can further break it up into elements that compose the navigation. Focus on the element on the far left, then work your way right. When the navigation bar is positioned, move on to the section below it.

![Image of the design with horizontal lines dividing up each section](./slices.png)

![Image of the navigation with vertical lines dividing up each section](./slices2.png)

### Add the Visual Styles

Style the elements by referring to your design and adding the same font faces, font sizes, font weights, colors, and images. When translating the design to the web, you may need to make adjustments because the rendering in the browser may be different.

### Make it Responsive

Make it responsive so users on various screen sizes can view your content easily. Making a site responsive does not need to be complex. It does not have to shift elements around or have a mobile-only menu. A simple way to make a basic design responsive is to have a breakpoint when the elements start to get cut-off or start to shrink, make the elements stack, and make them the full width of the page.

![Desktop sized browser next to a mobile sized browser showing card elements switching to a stacked view](./responsive.png)

Now that you have your design implemented, you are done! From here you can decide what you want to do with it.

## 🎉 Next Steps

- Now that you have the site designed and coded, you can add content and host it.

  ![Browser screenshot of my live website](./site.png)

- You can continue to use it as a learning project by building a back-end or adding the design to a front-end framework.
- Keep practicing, keep refining. It can take time to get comfortable designing. I hope this guide was a step in the right direction.

## 🎯 Summary

- Designing a website yourself will help you practice design skills and help your website stand out.
- Create a wireframe to structure the content and functionality.
- Turn the wireframe into a visual design using a design tool. Get inspiration from the designs that you like.
- Code the HTML structure of the page to help understand what CSS needs to be applied to transform those elements.
- Style the page with CSS to match your design.
- Take it to the next level by deploying it, using it as a practice project, or continuing to refine your design.