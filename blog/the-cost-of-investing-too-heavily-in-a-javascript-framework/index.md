---
title: The Cost of Investing Too Heavily in a JavaScript Framework
date: 2020-01-13
description: My experiences in exploring front-end frameworks and how I landed on a future-friendly approach.
coverImage: cover.png
coverImageAltText: Front-end framework logos with cowboy hats on
---

## 🤠 "What Framework Should I Learn?"

Trying to decide what framework to learn is a common question, but there is no clear winner. The common, uncontroversial answer is "You can't go wrong, they are all great frameworks." They are indeed all great, but I think you _can_ go wrong by investing too heavily in a framework.

I struggled with deciding what framework to learn and jumped back and forth between them for a while. My concern was that learning a specific framework would limit me to only using that framework. I could not confidently advocate for a framework knowing the churn of the JavaScript ecosystem. I wanted to learn something that could transcend the divisions between frameworks. Learning is an investment and I'm not a gambler, are you? 🃏

## 📊 Framework Fragmentation

Libraries and tools evolving is the natural progression of the web, but framework usage changing over time creates fragmentation among websites. There are sites built with old libraries such as MooTools, jQuery, and Backbone that are still in production today. Those sites will continue to work, but using them may not be preferred by developers or the libraries may not receive any new updates. Also, the components written for older libraries cannot be used in modern libraries.

Similarly, components built in a modern framework cannot be used in another. There is a split between modern front-end frameworks like React, Vue, and Angular that goes beyond developer preference. A full component library created in React cannot be easily ported to Angular.

- _"What's the problem? We can just rebuild those old Angular apps to use React."_
  - You could, but rewrites can be costly. The Angular apps work fine and another team in your company may prefer using Angular for that project. **If it ain't broke, don't fix it.**
- _"Okay, we'll just implement the same components in Angular and React."_
  - Now you have two codebases that do the same thing. Any updates are double the work or you run the risk of those codebases becoming out of sync. **Don't repeat yourself.**
- _"Maybe we can separate the styles from our components and import in React, Angular, or any other framework we decide to use."_
  - That sounds difficult to maintain and you would still have to repeat yourself. You have to reconstruct most of the component's markup and functionality. Why use a custom solution to do that? **Keep it simple.**
  - This was the approach taken by Google's Material Components for React, we will see how that worked out later in the article.
- _"Well then what do you suggest?"_
  - **Use what the web platform gives you. 🌐**

## 📚 Web Standards

Around the time I was researching what framework to learn, others were advocating for using a standards-based approach with Web Components. The reasons for Web Components sounded appealing to me and would address my concerns about being locked into a framework that may not survive the test of time. The problems I ran into were the negative opinions surrounding them and how to use Web Components effectively. I found that the initial pitch was appealing but trying to put it into practice was confusing and left me with a lot of questions.
  - Do I write plain standards-compliant JavaScript?
  - Do I use a basic library to make it easier? How do I build a full web application with it?
  - Do I use a library to compile to standard Web Components?
  - There are a lot of libraries, how do I choose?
  - Wait, why am I researching different Web Component libraries, isn't this what I wanted to avoid?

I tried to answer these questions in my research and wanted to share what I learned here.

## 🌉 Build on a Solid Foundation

I started learning basic HTML, CSS, and JavaScript 20 years ago and most of the fundamentals are still valid today. The web moves slowly. Standards that are implemented in the browser will not change overnight. There are committees to decide what features to add. Each feature goes through stages before it is officially part of a standard. Because of this, new standards move slowly and obsolete features may never be officially deprecated, so they will continue to work for backward compatibility.

That is still the case today. The standards that make Web Components will not disappear in 3-5 years. Can you say the same for your framework of choice?

## 🐎 The Framework Wild West

Frameworks are not bound to a standards body, which means the framework can introduce breaking changes at any time. An example is transitioning from Angular 1 to Angular 2. Existing Angular apps required a significant refactor to be ported over to Angular 2. The Angular community was upset about that but there was never any agreement or guarantee that things would always be the same. 

Facebook builds React for Facebook. If they need to change it, they can do that. Just because the code is open source does not mean that their interests are open source. Again, there is no agreement or guarantee that they won't pull the rug out from under you.

Frameworks can also become unsupported over time. You can still use old frameworks and they will work, but eventually, there will not be any more updates to the framework or updates to community-maintained packages. That can affect a team's productivity if a bug in the framework or package will not be fixed. This could also affect the potential to attract new developers and keep them if developers do not want to be stuck using an outdated tool.

## ⚙ Not-so Reusable Components

Reusable component libraries written for React or Vue only work in those frameworks. If your company implements a design system and uses React for the component library, what happens when a new project decides not to use React? What happens when the next React introduces fundamental changes or the next big thing is a different library entirely? That polished component library that your team spent years building will not work in the new framework. If you want to continue using it, you will be stuck on older technology. If you want to move to new tools, that component library needs to be rewritten.

Google began building reusable components for Material Design. The React version used an approach of importing styles from Material Components Web (which is the Bootstrap-like version, not the Web Component version). The .scss file for each React component typically only contained an import statement that imported the styles it needed. Then each component was re-written with everything else that could not be imported into React components such as markup, properties, and component state. This approach was problematic because if the core of Material Components Web changed, the React version may have also needed to be fixed or updated to accommodate those changes. I think the team behind it began to realize that these should be fully reusable Web Components and decided to focus on those because they are independent of any framework. They passed off the React version to the community.
>_"MDC-React is no longer under active development.  
We created MDC-React in 2018 to implement the updated Material Design guidelines. Since then, the open-source React community has embraced the new guidelines and created a number of excellent unofficial implementations. See Material Design Components - Web Framework Wrappers for a partial list.  
**In order to increase our focus on implementing our core, framework-independent libraries ([MDC-Web](https://github.com/material-components/material-components-web) and [MWC](https://github.com/material-components/material-components-web-components))**, we’re passing the Material+React baton back to the community. That means Material Design will no longer be updating and maintaining this repo. We recommend that you switch to another implementation and keep building beautiful, usable apps based on Material Design. Thanks for being part of the project!" - [Google's Material Components for React](https://github.com/material-components/material-components-web-react)_

I would not bet on React being the top framework a few years from now, but I would confidently bet that the browser standards will remain backward compatible. With frameworks like Vue and Svelte on the rise, can you confidently say that you will be writing React code in a few years?

## 🔮 Web Standards Will Not Replace Frameworks (And That's Okay)

I'm not suggesting ditching frameworks for Web Components. Frameworks are good because they can freely innovate where browser standards cannot. The aim of using a standards-based approach is not to replace frameworks, it is to augment them.
- Build your reusable component library and use it for the foreseeable future, **no matter what framework**. Or use it **without a framework**.
- Teams across your organization do not have to implement the design system in their framework, they can use their framework of choice and use the existing component library.

>_"React and Web Components are built to solve different problems. Web Components provide strong encapsulation for reusable components, while React provides a declarative library that keeps the DOM in sync with your data. The two goals are complementary." - [React Documentation](https://reactjs.org/docs/web-components.html)._

**Web Components + Frameworks, not Web Components vs. Frameworks**

## 🔧 Embrace the Tools

Web Components by themselves are not good enough. You might be thinking, "Aha! Frameworks are better!", but not so fast. Standard Web Components are missing some key features and the modern developer experience that framework users have come to expect. This is a common criticism of Web Components. Luckily, there are tools out there to help ease these pain points.

Because there are many libraries to choose from, tools built to target web standards may also seem fragmented. There is Stencil, LitElement, other lesser-known tools, or standard Web Components. The key difference between Web Component tools and proprietary frameworks is that the output produced by the Web Component tools will be more future-proof. If your team decides to use React, then switches to Svelte for another project, Web Components _can_ make that transition without those components being re-written. If your team needs to put together a simple app, Web Components can also be used _without any_ framework.

The idea is not to go completely standard and stop using tools because there are too many, the goal should be to target web standards to help make the work you are doing more future-friendly. This will result in fewer rewrites because those components will work anywhere. This will also give you the flexibility to migrate to the next big framework because your existing components can come with you.

## 🎯 Summary

- You can't go wrong with any modern front-end framework, but you can go wrong by investing too heavily in them.
- Having choices in frameworks helps move the web forward but it also fragments it. The hot framework today may not be the hot framework tomorrow.
- By investing in web standards, your front-end will be more future-friendly.
- Framework authors are not bound by standards, they might pull the rug out from under you.
- Reusable component libraries that are built in a particular framework are only reusable within that framework.
- Web Components will not replace frameworks, but they complement them nicely. You can have reusable components and use the latest and greatest frameworks.

## 📖 Resources

### Libraries

- **[Stencil](https://stenciljs.com/)** - Stencil is a tool for building reusable design systems. It generates standards-based Web Components and provides a virtual DOM, asynchronous rendering, reactive data-binding, TypeScript, and JSX. This was the top write-in answer for the [2019 State of JavaScript Survey - Front End Category](https://2019.stateofjs.com/front-end-frameworks/other-tools/).
- **[LitElement](https://lit-element.polymer-project.org/)** - LitElement is a base class to make it easier to create Web Components. It is ideal for sharing elements across your organization or building a design system. Use your components in an HTML page or a framework like React or Vue. This was the number two write-in answer for the [2019 State of JavaScript Survey - Front End Category](https://2019.stateofjs.com/front-end-frameworks/other-tools/).

### Articles & Links

- **[DEV Community: Apple Just Shipped Web Components to Production and You Probably Missed It](https://dev.to/ionic/apple-just-shipped-web-components-to-production-and-you-probably-missed-it-57pf)**
- **[DEV Community: Web Components: from zero to hero](https://dev.to/thepassle/web-components-from-zero-to-hero-4n4m)**
- **[DEV Community: Lets Build Web Components! Part 1: The Standards](https://dev.to/bennypowers/lets-build-web-components-part-1-the-standards-3e85)**
- **[DEV Community: Why the React community is missing the point about Web Components](https://dev.to/ben/why-the-react-community-is-missing-the-point-about-web-components-1ic3)**
- **[DEV Community: Why I don't use Web Components](https://dev.to/richharris/why-i-don-t-use-web-components-2cia)** - Opposing opinion from Rich Harris (the creator of Svelte) that generated some good discussion.
- **[DEV Community: Stencil: I Think I Found My Frontend Home](https://dev.to/deciduously/stencil-i-think-i-found-my-frontend-home-46bf)**
- **[Custom Elements Everywhere](https://custom-elements-everywhere.com/)** - A site that tests custom element support in frameworks. Most frameworks have great support but the main holdout is React. 
- **[WebComponents.dev](https://webcomponents.dev/)** - An online editor to easily start a Web Component project. It offers standard Web Component or library starters to make it easy to try them out.

### Books

- **[Web Components in Action](https://www.manning.com/books/web-components-in-action)** - This book covers how Web Components are a standardized way to build reusable custom elements. It includes how they are encapsulated to keep their internal structure separate from other elements. It goes through designing, building, and deploying reusable standard Web Components.

### Podcasts

- **[Bet on the Web](https://betontheweb.ionicframework.com/)** - A podcast from the Ionic team about the web platform.

### Talks

- **[Web Components: The Secret Ingredient Helping Power The Web](https://www.youtube.com/watch?v=YBwgkr_Sbx0)**
- **[Using Web Components in Ionic (Polymer Summit 2017)](https://www.youtube.com/watch?v=UfD-k7aHkQE)**
- **[Stencil: a build-time approach to the web by Manu Martinez-Almeida | JSConf EU 2019](https://www.youtube.com/watch?v=M1F81V-NhP0)**
- **[Custom Elements Everywhere (Polymer Summit 2017)](https://www.youtube.com/watch?v=sK1ODp0nDbM)**